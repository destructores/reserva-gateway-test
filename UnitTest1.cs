﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;
using System.IO;
using System.Text;
using System.Web.Script.Serialization;
using ServicesInventory.Dominio;
using System.Collections.Generic;

namespace ReservaGatewayTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void UsuarioDNITest()
        {
            // Prueba de creación de alumno vía HTTP POST
            byte[] data = Encoding.UTF8.GetBytes("");
            try
            {

                HttpWebRequest req2 = (HttpWebRequest)WebRequest.Create("http://localhost:53506/ReservaSalasGateway.svc/Usuarios/403212");
                req2.Method = "GET";
                HttpWebResponse res2 = (HttpWebResponse)req2.GetResponse();
                StreamReader reader2 = new StreamReader(res2.GetResponseStream());
                string usuarioJson = reader2.ReadToEnd();
               JavaScriptSerializer js2 = new JavaScriptSerializer();
               Usuario servicioObtenido = js2.Deserialize<Usuario>(usuarioJson);
               Assert.AreEqual(servicioObtenido.Dni, 403212);
            }
            catch (WebException e)
            {
                System.Console.WriteLine(e.Message);
            }
        }

        [TestMethod]
        public void UsuarioDNIExceptionTest()
        {
            // Prueba de creación de alumno vía HTTP POST
            byte[] data = Encoding.UTF8.GetBytes("");
            try
            {

                HttpWebRequest req2 = (HttpWebRequest)WebRequest.Create("http://localhost:53506/ReservaSalasGateway.svc/Usuarios/403212222");
                req2.Method = "GET";
                HttpWebResponse res2 = (HttpWebResponse)req2.GetResponse();
                StreamReader reader2 = new StreamReader(res2.GetResponseStream());
                string usuarioJson = reader2.ReadToEnd();
                JavaScriptSerializer js2 = new JavaScriptSerializer();
                Usuario servicioObtenido = js2.Deserialize<Usuario>(usuarioJson);
                Assert.AreEqual(servicioObtenido.Dni, 403212);
            }
            catch (WebException e)
            {
                HttpStatusCode code = ((HttpWebResponse)e.Response).StatusCode;
                string message = ((HttpWebResponse)e.Response).StatusDescription;
                StreamReader reader = new StreamReader(e.Response.GetResponseStream());
                string error = reader.ReadToEnd();
                JavaScriptSerializer js = new JavaScriptSerializer();
                string mensaje = js.Deserialize<string>(error);
                Assert.AreEqual("No existe usuario", mensaje);
            }
        }

        [TestMethod]
        public void ListaUsuarioTest()
        {
            // Prueba de creación de alumno vía HTTP POST
            byte[] data = Encoding.UTF8.GetBytes("");
            try
            {

                HttpWebRequest req2 = (HttpWebRequest)WebRequest.Create("http://localhost:53506/ReservaSalasGateway.svc/Usuarios");
                req2.Method = "GET";
                HttpWebResponse res2 = (HttpWebResponse)req2.GetResponse();
                StreamReader reader2 = new StreamReader(res2.GetResponseStream());
                string usuarioJson2 = reader2.ReadToEnd();
                JavaScriptSerializer js2 = new JavaScriptSerializer();
                
                List<Usuario> usuarioObtenido = js2.Deserialize<List<Usuario>>(usuarioJson2);
                Assert.AreEqual(usuarioObtenido.Count > 0, true);
            }
            catch (WebException e)
            {
                System.Console.WriteLine(e.Message);
            }
        }


        [TestMethod]
        public void ObtenerSalaZonaTest()
        {
            // Prueba de creación de alumno vía HTTP POST
            byte[] data = Encoding.UTF8.GetBytes("");
            try
            {

                HttpWebRequest req2 = (HttpWebRequest)WebRequest.Create("http://localhost:53506/ReservaSalasGateway.svc/Salas/SANMIGUEL");
                req2.Method = "GET";
                HttpWebResponse res2 = (HttpWebResponse)req2.GetResponse();
                StreamReader reader2 = new StreamReader(res2.GetResponseStream());
                string salaJson2 = reader2.ReadToEnd();
                JavaScriptSerializer js2 = new JavaScriptSerializer();

                List<Sala> salaObtenida = js2.Deserialize<List<Sala>>(salaJson2);

                Assert.AreEqual((salaObtenida.Count > 0), true);

            }
            catch (WebException e)
            {
                System.Console.WriteLine(e.Message);
            }
        }

        [TestMethod]
        public void ObtenerSalaZonaExceptionTest()
        {
            // Prueba de creación de alumno vía HTTP POST
            byte[] data = Encoding.UTF8.GetBytes("");
            try
            {

                HttpWebRequest req2 = (HttpWebRequest)WebRequest.Create("http://localhost:53506/ReservaSalasGateway.svc/Salas/SANMIGUELss");
                req2.Method = "GET";
                HttpWebResponse res2 = (HttpWebResponse)req2.GetResponse();
                StreamReader reader2 = new StreamReader(res2.GetResponseStream());
                string salaJson2 = reader2.ReadToEnd();
                JavaScriptSerializer js2 = new JavaScriptSerializer();

                List<Sala> salaObtenida = js2.Deserialize<List<Sala>>(salaJson2);

                Assert.AreEqual((salaObtenida.Count > 0), true);

            }
            catch (WebException e)
            {
                HttpStatusCode code = ((HttpWebResponse)e.Response).StatusCode;
                string message = ((HttpWebResponse)e.Response).StatusDescription;
                StreamReader reader = new StreamReader(e.Response.GetResponseStream());
                string error = reader.ReadToEnd();
                JavaScriptSerializer js = new JavaScriptSerializer();
                string mensaje = js.Deserialize<string>(error);
                Assert.AreEqual("No existe sala", mensaje);
                
            }
        }

        [TestMethod]
        public void ObtenerSalasTest()
        {
            // Prueba de creación de alumno vía HTTP POST
            byte[] data = Encoding.UTF8.GetBytes("");
            try
            {

                HttpWebRequest req2 = (HttpWebRequest)WebRequest.Create("http://localhost:53506/ReservaSalasGateway.svc/Salas");
                req2.Method = "GET";
                HttpWebResponse res2 = (HttpWebResponse)req2.GetResponse();
                StreamReader reader2 = new StreamReader(res2.GetResponseStream());
                string salaJson2 = reader2.ReadToEnd();
                JavaScriptSerializer js2 = new JavaScriptSerializer();

                List<Sala> salaObtenida = js2.Deserialize<List<Sala>>(salaJson2);

                Assert.AreEqual((salaObtenida.Count > 0), true);

            }
            catch (WebException e)
            {
                System.Console.WriteLine(e.Message);
            }
        }
    }
}
