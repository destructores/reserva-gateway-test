﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ServicesInventory.Dominio
{

    public class Sala
    {

        public int Id { get; set; }

        public string Nombre { get; set; }

  
        public string Direccion { get; set; }

        public string Referencia { get; set; }

   
        public int Capacidad { get; set; }

        public string Contacto { get; set; }

        public string Correo { get; set; }

        public int Telefono { get; set; }

  
        public string Distrito { get; set; }

        public string Zona { get; set; }
    }
}