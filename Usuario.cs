﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ServicesInventory.Dominio
{

    public class Usuario
    {

        public int Dni { get; set; } //Declarando la clase pública Dni y su método get

        public string Nombre { get; set; }//Declarando la clase pública Nombre y su método get


        public string Apellido { get; set; }//Declarando la clase pública Apellido y su método get

        public int TelefonoMovil { get; set; }//Declarando la clase pública TelefonoMovil y su método get


        public string Zona { get; set; }//Declarando la clase pública Zona y su método get
    }
}